package com.example.sample

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import java.util.concurrent.locks.Condition

private const val DB_NAME = "FoodManage"
private const val DB_VERSION = 1

class SampleDBOpenHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        //テーブルの作成
        db?.execSQL(createIngredientTable())
        db?.execSQL(createFoodTable())
        db?.execSQL(createRecordTable())
        db?.execSQL(createMyCondateTable())
        db?.execSQL(createCategoryTable())
        db?.execSQL(createFoodIngredientsTable())
        db?.execSQL(createMyCondateFoodTable())
        db?.execSQL(createUserInfoTable())
//        db?.execSQL("CREATE TABLE texts ( " +
//                " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                " text TEXT NOT NULL, " +
//                " created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //バージョン更新時のSQL発行
    }

    fun insertIngredient(ingredient: Ingredient): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.Ingredient.NAME, ingredient.name)
            put(DBContract.Ingredient.SUGAR, ingredient.sugar)
            put(DBContract.Ingredient.FAT, ingredient.fat)
            put(DBContract.Ingredient.VITAMIN, ingredient.vitamin)
            put(DBContract.Ingredient.MINERAL, ingredient.mineral)
            put(DBContract.Ingredient.FIBER, ingredient.fiber)
            put(DBContract.Ingredient.CALORIE, ingredient.calorie)
            put(DBContract.Ingredient.UNIT, ingredient.unit)
            put(DBContract.Ingredient.ALLERGEN, ingredient.allergen)
        }
        //データベースに挿入する
        db.insert(DBContract.Ingredient.TABLE_NAME, null, record)

        return true
    }

    fun insertFood(food: Food): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {

            put(DBContract.Food.NAME, food.name)
            put(DBContract.Food.FAVORITE, food.favorite)
            put(DBContract.Food.MEMO, food.memo)
        }
        //データベースに挿入する
        db.insert(DBContract.Food.TABLE_NAME, null, record)

        return true
    }


    fun insertRecord(record: Record): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.Record.FOOD_ID, record.food_id)
            put(DBContract.Record.YEAR, record.year)
            put(DBContract.Record.MONTH, record.month)
            put(DBContract.Record.DATE, record.date)
            put(DBContract.Record.TIME, record.time)
        }
        //データベースに挿入する
        db.insert(DBContract.Record.TABLE_NAME, null, record)

        return true
    }

    fun insertMyCondate(myCondate: MyCondate): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.MyCondate.NAME, myCondate.name)
        }
        //データベースに挿入する
        db.insert(DBContract.MyCondate.TABLE_NAME, null, record)

        return true
    }

    fun insertCategory(category: Category): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.Category.NAME, category.name)
            put(DBContract.Category.HIGHER_ID, category.higher_id)
        }
        //データベースに挿入する
        db.insert(DBContract.Category.TABLE_NAME, null, record)

        return true
    }

    fun insertFood_Ingredient(food_Ingredient: Food_Ingredient): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.Foods_Ingredients.FOOD_ID, food_Ingredient.food_id)
            put(DBContract.Foods_Ingredients.INGREDIENT_ID, food_Ingredient.Ingredient_id)
            put(DBContract.Foods_Ingredients.NUMBER, food_Ingredient.num)
        }
        //データベースに挿入する
        db.insert(DBContract.Foods_Ingredients.TABLE_NAME, null, record)
        return true
    }

    fun insertMyCondate_Food(myCondate_Food: MyCondate_Food): Boolean {
        //書き込み可能なデータベースを開く
        val db = writableDatabase

        //挿入するレコード
        val record = ContentValues().apply {
            put(DBContract.MyCondate_Foods.MYCONDATE_ID, myCondate_Food.MyCondate_id)
            put(DBContract.MyCondate_Foods.FOOD_ID, myCondate_Food.MyCondate_id)
            put(DBContract.MyCondate_Foods.NUMBER, myCondate_Food.num)
        }
        //データベースに挿入する
        db.insert(DBContract.MyCondate_Foods.TABLE_NAME, null, record)
        return true
    }

    fun searchRaw(tableName: String, column: String, condition: String) {
        //書き込み可能なデータベースを開く
        val db = readableDatabase
        //データベースから全件検索する
        val cursor = db.query(
            tableName, null, null, null, null, null, null
        )

        cursor.use {
            //カーソルで順次処理していく
            while (cursor.moveToNext()) {
                //保存されているテキストを得る
                    val raw = cursor.getString(cursor.getColumnIndex(column))
                    raws.add(raw)
                }
            }
        }
    }
}

//テーブルから保存されているテキストを検索する関数
fun queryTexts(context: Context): List<String> {
    //読み込み用のデータベースを開く
    val database = SampleDBOpenHelper(context).readableDatabase
    //データベースから全件検索する
    val cursor = database.query(
        "texts", null, null, null, null, null, "created_at DESC"
    )

    val texts = mutableListOf<String>()
    cursor.use {
        //カーソルで順次処理していく
        while (cursor.moveToNext()) {
            //保存されているテキストを得る
            val text = cursor.getString(cursor.getColumnIndex("text"))
            texts.add(text)
        }
    }

    database.close()
    return texts
}

//テーブルにレコードを挿入する関数
fun insertText(tableName: String, context: Context, text: String) {
    //書き込み可能なデータベースを開く
    val database = SampleDBOpenHelper(context).writableDatabase

    database.use { db ->
        //挿入するレコード
        val record = ContentValues().apply {
            put("text", text)
        }
        //データベースに挿入する
        db.insert(tableName, null, record)
    }
}